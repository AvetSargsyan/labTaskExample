import {Locator, Page} from "@playwright/test";

export class BasePage {
  private readonly page: Page;
  readonly loader: Locator;

  constructor(page: Page) {
    this.page = page;
    this.loader = page.locator("[class^=spinningPreloader__spinning]");
  }

  async goto(url: string, options?: {
    referer?: string,
    timeout?: number,
    waitUntil?: "load" | "domcontentloaded" | "networkidle" | "commit"
  }): Promise<void> {
    await this.page.goto(url, options ? options : {waitUntil: "load"});
  }

  async url(): Promise<string> {
    return this.page.url();
  }

  async waitForTimeout(timeout: number): Promise<void> {
    await this.page.waitForTimeout(timeout);
  }
}
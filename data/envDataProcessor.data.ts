import dotenv from "dotenv";

dotenv.config();

class EnvDataProcessor {
  readonly baseUrl: string;
  readonly user: string;
  readonly userName: string;
  readonly userPass: string;
  readonly memberName: string;
  readonly memberPass: string;
  readonly authToken: string;
  readonly env: string;
  readonly LT_USERNAME: string;
  readonly LT_ACCESS_KEY: string

  constructor() {
    this.baseUrl = process.env.URL!;
    this.user = process.env.USER!;
    this.userName = process.env.USER_NAME!;
    this.userPass = process.env.USER_PASS!;
    this.memberName = process.env.MEMBER_NAME!;
    this.memberPass = process.env.MEMBER_PASS!;
    this.authToken = process.env.AUTH_TOKEN!;
    this.env = process.env.ENV!;
    this.LT_USERNAME = process.env.LT_USERNAME!;
    this.LT_ACCESS_KEY = process.env.LT_ACCESS_KEY!;
  }
}

export default new EnvDataProcessor();

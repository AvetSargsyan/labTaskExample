import {test, expect} from "@playwright/test";

import {POFactory} from "../../pages/pageFactory";
import {urlData} from "../../data/url.data";
import {titles} from "../../data/titles.data";
import {credentialsData} from "../../data/credentials.data";
import {errorMessageData} from "../../data/errorMessage.data";

let loginPage: ReturnType<POFactory["getLoginPage"]>;

test.beforeEach(async ({page}) => {
  loginPage = new POFactory(page).getLoginPage();

  await test.step("Open Forgot Password component", async () => {
    await loginPage.goto(urlData.forgotPassword);
    await loginPage.forgotPasswordComponent.header.waitFor();
  });
});

test.describe("Check Forgot Password component @integration", () => {
  test("Check elements in component", async () => {
    await test.step("Check Forgot password components", async () => {
      await expect(
        loginPage.forgotPasswordComponent.header,
        "Page header should be visible",
      ).toHaveText(titles.forgotPassword);
      await expect(
        loginPage.forgotPasswordComponent.emailInput,
        "Email input should be visible",
      ).toBeVisible();
      await expect(
        loginPage.forgotPasswordComponent.sendEmailBtn,
        "Send email button should be visible",
      ).toBeVisible();
      await expect(
        loginPage.forgotPasswordComponent.cancelBtn,
        "Cancel button should be visible",
      ).toBeVisible();
      await expect(
        loginPage.forgotPasswordComponent.privqcyPolicyLink,
        "Privacy policy link should be visible",
      ).toBeVisible();
    });
  })

  test("Check error message for not registered email", async () => {
    await test.step("Input fake mail and proceed", async () => {
      await loginPage.forgotPasswordComponent.emailInput.fill(
        credentialsData.invalidUser.email,
      );
      await loginPage.forgotPasswordComponent.sendEmailBtn.click({force: true});
      await loginPage.loader.waitFor();
    });

    await expect(
      loginPage.forgotPasswordComponent.errorMessage,
      "Error message text should be correct",
    ).toContainText(credentialsData.invalidUser.email);
  });

  test("Check form validation error", async () => {
    const invalidData = ['', 'someFakeData', 'fakeMail@a.aa', 'fakeMail@aa.a'];

    for (const data in invalidData) {
      await test.step("Input fake mail and proceed", async () => {
        await loginPage.forgotPasswordComponent.emailInput.fill(
          data
        );
        await loginPage.forgotPasswordComponent.sendEmailBtn.click({force: true});
        await loginPage.forgotPasswordComponent.inputErrorMessage.waitFor();
      });

      await expect(
        loginPage.forgotPasswordComponent.inputErrorMessage,
        "Input error message text should be correct",
      ).toContainText(errorMessageData.forgotPasswordInputError);
    }
  });
})

import {defineConfig, devices} from "@playwright/test";
import envDataProcessorData from "./data/envDataProcessor.data";


export default defineConfig({
  testDir: "tests",
  fullyParallel: true,
  forbidOnly: !!process.env.CI,
  retries: envDataProcessorData.env === "CI" ? 2 : 0,
  workers: envDataProcessorData.env === "CI" ? 3 : 1,
  reporter: [
    ["line"],
    [
      "allure-playwright",
      {
        outputFolder: "reports/allure-results",
        suiteTitle: true,
        detail: true,
        environmentInfo: {},
      },
    ],
  ],
  use: {
    headless: envDataProcessorData.env === "CI",
    trace: "on-first-retry",
  },

  projects: [
    {
      name: "integration",
      grep: /@integration/,
      use: {...devices["Desktop Chrome"]},
    },
    {
      name: "e2e",
      grep: /@e2e/,
      use: {...devices["Desktop Chrome"]},
    }
  ],
});
